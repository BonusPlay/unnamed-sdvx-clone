#include "stdafx.h"
#include "TestTickable.hpp"
#include "Global.hpp"
#include "GUI/Button.hpp"
#include "GUI/TextInputField.hpp"
#include "GUI/Spinner.hpp"
#include <GUI/Slider.hpp>

bool TestTickable::Init()
{
	m_guiStyle = g_commonGUIStyle;

	m_gamepad = g_gameWindow->OpenGamepad(0);

	{
		auto box0 = std::make_shared<ScrollBox>(m_guiStyle);
		Canvas::Slot* slot = g_rootCanvas->Add(box0);
		slot->anchor = Anchor(0.0f, 0.0f);
		slot->offset = Rect(Vector2(10.0f, 10.0f), Vector2(500, 500.0f));
		slot->autoSizeX = false;
		slot->autoSizeY = false;

		{
			auto box = make_shared<LayoutBox>();
			box->layoutDirection = LayoutBox::Vertical;
			box0->SetContent(box);

			LayoutBox::Slot* btnSlot;
			auto btn0 = make_shared<Button>(m_guiStyle);
			btn0->SetText(L"TestButton0");
			btn0->SetFontSize(32);
			btnSlot = box->Add(btn0);
			btnSlot->padding = Margin(2);

			auto btn1 = make_shared<Button>(m_guiStyle);
			btn1->SetText(L"This is a button with slightly\nlarger text");
			btn1->SetFontSize(32);
			btnSlot = box->Add(btn1);
			btnSlot->padding = Margin(2);

			auto fld = make_shared<TextInputField>(m_guiStyle);
			fld->SetText(L"textinput");
			fld->SetFontSize(32);
			btnSlot = box->Add(fld);
			btnSlot->fillX = true;
			btnSlot->padding = Margin(2);

			auto sld = make_shared<Slider>(m_guiStyle);
			btnSlot = box->Add(sld);
			btnSlot->fillX = true;
			btnSlot->padding = Margin(2);

			sld = make_shared<Slider>(m_guiStyle);
			btnSlot = box->Add(sld);
			btnSlot->fillX = true;
			btnSlot->padding = Margin(2);

			sld = make_shared<Slider>(m_guiStyle);
			btnSlot = box->Add(sld);
			btnSlot->fillX = true;
			btnSlot->padding = Margin(2);

			// Duplicate items
			btn0 = make_shared<Button>(m_guiStyle);
			btn0->SetText(L"TestButton0");
			btn0->SetFontSize(32);
			btnSlot = box->Add(btn0);
			btnSlot->padding = Margin(2);

			btn1 = make_shared<Button>(m_guiStyle);
			btn1->SetText(L"This is a button with slightly\nlarger text");
			btn1->SetFontSize(32);
			btnSlot = box->Add(btn1);
			btnSlot->padding = Margin(2);

			fld = make_shared<TextInputField>(m_guiStyle);
			fld->SetText(L"textinput");
			fld->SetFontSize(32);
			btnSlot = box->Add(fld);
			btnSlot->fillX = true;
			btnSlot->padding = Margin(2);

			sld = make_shared<Slider>(m_guiStyle);
			btnSlot = box->Add(sld);
			btnSlot->fillX = true;
			btnSlot->padding = Margin(2);

			sld = make_shared<Slider>(m_guiStyle);
			btnSlot = box->Add(sld);
			btnSlot->fillX = true;
			btnSlot->padding = Margin(2);

			sld = make_shared<Slider>(m_guiStyle);
			btnSlot = box->Add(sld);
			btnSlot->fillX = true;
			btnSlot->padding = Margin(2);
		}
	}

	// Setting bar
	{
		auto sb = make_shared<SettingsBar>(m_guiStyle);
		m_settings = shared_ptr<SettingsBar>(sb);
		sb->AddSetting(&a, 0.0f, 1.0f, "A");
		sb->AddSetting(&b, 0.0f, 10.0f, "B");
		sb->AddSetting(&c, 0.0f, 5.0f, "C");
		sb->AddSetting(&d, -2.0f, 2.0f, "D");
		m_textSettings.Add("Setting 1");
		m_textSettings.Add("Set 2");
		m_textSettings.Add("3");
		sb->AddSetting(&e, m_textSettings, m_textSettings.size(), "E");

		Canvas::Slot* slot = g_rootCanvas->Add(sb);
		slot->anchor = Anchor(0.75f, 0.0f, 1.0f, 1.0f);
		slot->autoSizeX = false;
		slot->autoSizeY = false;
	}

	// Spinner
	{
		auto spinner = make_shared<Spinner>(m_guiStyle);
		Canvas::Slot* slot = g_rootCanvas->Add(spinner);
		slot->anchor = Anchor(0.9f, 0.9f);
		slot->autoSizeX = true;
		slot->autoSizeY = true;
		slot->alignment = Vector2(1.0f, 1.0f);
	}
	return true;
}

void TestTickable::OnKeyPressed(int32 key)
{
	if (key == SDLK_TAB)
		m_settings->SetShow(!m_settings->IsShown());
}