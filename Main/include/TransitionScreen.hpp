#pragma once
#include "ApplicationTickable.hpp"
#include "AsyncLoadable.hpp"
#include "Shared/Jobs.hpp"
#include "GUI/Canvas.hpp"

/*
	Loading and transition screen
*/
class TransitionScreen : public IApplicationTickable
{
public:
	TransitionScreen(unique_ptr<IAsyncLoadableApplicationTickable> next);
	~TransitionScreen();

	bool Init() override;
	void Tick(float deltaTime) override;

	bool DoLoad() const;
	void OnFinished(Job job);

private:
	shared_ptr<Canvas> m_loadingOverlay;
	unique_ptr<IAsyncLoadableApplicationTickable> m_tickableToLoad;
	Job m_loadingJob;

	float m_transitionTimer = 0;
};
