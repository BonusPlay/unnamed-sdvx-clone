#include "stdafx.h"
#include "Application.hpp"
#include "Global.hpp"
#include "TestTickable.hpp"
#include "TransitionScreen.hpp"
#include "Game.hpp"
#include "GL/glew.h"
#include "SongSelect.hpp"
#include "TitleScreen.hpp"

struct TickableChange
{
	enum Mode
	{
		Added,
		Removed,
	};

	Mode mode;
	unique_ptr<IApplicationTickable> tickable;
};

static float g_avgRenderDelta = 0.0f;

Application::Application()
{
	// Enforce single instance
	assert(!g_application);
}

Application::~Application()
{
	ProfilerScope $("Application Cleanup");

	// Cleanup input
	g_input.Cleanup();

	// Finally, save config
	m_SaveConfig();
}

bool Application::Run()
{
	if (!m_Init())
		return false;

	if (m_commandLine.Contains("-test"))
	{
		// Create test scene
		AddTickable(make_unique<TestTickable>());
	}
	else
	{
		bool mapLaunched = false;
		// Play the map specified in the command line
		if (m_commandLine.size() > 1 && m_commandLine[1].front() != '-')
		{
			auto game = make_unique<Game>(m_commandLine[1]);
			AddTickable(make_unique<TransitionScreen>(std::move(game)));
			if (!game)
			{
				Logf("LaunchMap(%s) failed", Logger::Error, m_commandLine[1]);
			}
			else
			{
				auto& cmdLine = g_application->GetAppCommandLine();
				if (cmdLine.Contains("-autoplay") || cmdLine.Contains("-auto"))
				{
					game->GetScoring().autoplay = true;
				}
				mapLaunched = true;
			}
		}

		if (!mapLaunched)
		{
			if (m_commandLine.Contains("-notitle"))
				AddTickable(make_unique<SongSelect>());
			else // Start regular game, goto title screen
				AddTickable(make_unique<TitleScreen>());
		}
	}

	m_MainLoop();

	return true;
}

void Application::SetCommandLine(int32 argc, char** argv)
{
	m_commandLine.clear();

	// Split up command line parameters
	for (int32 i = 0; i < argc; i++)
	{
		m_commandLine.Add(argv[i]);
	}
}

void Application::SetCommandLine(const char* cmdLine)
{
	m_commandLine.clear();

	// Split up command line parameters
	m_commandLine = Path::SplitCommandLine(cmdLine);
}

bool Application::m_LoadConfig()
{
	File configFile;
	if (configFile.OpenRead("Main.cfg"))
	{
		FileReader reader(configFile);
		if (g_gameConfig.Load(reader))
			return true;
	}
	else g_gameConfig.Save("Main.cfg");
	return false;
}

void Application::m_SaveConfig()
{
	if (!g_gameConfig.IsDirty())
		return;

	File configFile;
	if (configFile.OpenWrite("Main.cfg"))
	{
		FileWriter writer(configFile);
		g_gameConfig.Save(writer);
	}
}

bool Application::m_Init()
{
	ProfilerScope $("Application Setup");

	// Must have command line
	assert(!m_commandLine.empty());

	if (!m_LoadConfig())
		Logf("Failed to load config file", Logger::Warning);

	// Job sheduler
	g_jobSheduler = make_shared<JobSheduler>();

	m_allowMapConversion = false;
	bool debugMute = false;
	bool startFullscreen = false;

	// Fullscreen settings from config
	if (g_gameConfig.GetBool(GameConfigKeys::Fullscreen))
		startFullscreen = true;

	uint32 fullscreenMonitor = g_gameConfig.GetInt(GameConfigKeys::FullscreenMonitorIndex);

	for (auto& cl : m_commandLine)
	{
		String k, v;
		if (cl.Split("=", &k, &v))
		{
			if (k == "-monitor")
				fullscreenMonitor = strtol(*v, nullptr, 0);
		}
		else
		{
			if (cl == "-convertmaps")
				m_allowMapConversion = true;
			else if (cl == "-mute")
				debugMute = true;
			else if (cl == "-fullscreen")
				startFullscreen = true;
		}
	}

	// Create the game window
	g_resolution = Vector2i(
		g_gameConfig.GetInt(GameConfigKeys::ScreenWidth),
		g_gameConfig.GetInt(GameConfigKeys::ScreenHeight));
	g_aspectRatio = static_cast<float>(g_resolution.x) / static_cast<float>(g_resolution.y);
	g_gameWindow = make_shared<Window>(g_resolution);
	g_gameWindow->Show();
	g_gameWindow->OnKeyPressed.Add(this, &Application::m_OnKeyPressed);
	g_gameWindow->OnKeyReleased.Add(this, &Application::m_OnKeyReleased);
	g_gameWindow->OnResized.Add(this, &Application::m_OnWindowResized);

	// Initialize Input
	g_input.Init(*g_gameWindow);

	// Set skin variable
	m_skin = g_gameConfig.GetString(GameConfigKeys::Skin);

	// Window cursor
	try
	{
		const Image cursorImg = make_shared<ImageRes>("skins/" + m_skin + "/textures/cursor.png");
		g_gameWindow->SetCursor(cursorImg, Vector2i(5, 5));
	}
	catch (std::runtime_error& err)
	{
		Logf("Failed to create cursor %s", Logger::Error, err.what());
	}

	if (startFullscreen)
		g_gameWindow->SwitchFullscreen(fullscreenMonitor);

	// Set render state variables
	m_renderStateBase.aspectRatio = g_aspectRatio;
	m_renderStateBase.viewportSize = g_resolution;
	m_renderStateBase.time = 0.0f;

	{
		ProfilerScope $1("Audio Init");

		// Init audio
		try
		{
			g_audio = make_unique<Audio>();
		}
		catch (std::runtime_error& err)
		{
			Logf("Audio initialization failed %s", Logger::Error, err.what());
			return true;
		}

		// Debug Mute?
		// Test tracks may get annoying when continously debugging ;)
		if (debugMute)
		{
			g_audio->SetGlobalVolume(0.0f);
		}
	}

	{
		ProfilerScope $("Online Init");
		g_online = make_unique<OnlineManager>();
	}

	{
		ProfilerScope $1("GL Init");

		// Create graphics context
		g_gl = make_shared<OpenGL>();
		if (!g_gl->Init(*g_gameWindow))
		{
			Logf("Failed to create OpenGL context", Logger::Error);
			return false;
		}
	}

	g_gameWindow->SetVSync(g_gameConfig.GetInt(GameConfigKeys::VSync));

	{
		ProfilerScope $1("GUI Init");

		// GUI Rendering
		try
		{
			g_guiRenderer = make_shared<GUIRenderer>(g_gl, g_gameWindow.get(), m_skin);
		}
		catch (const runtime_error& err)
		{
			Logf("Failed to initialize GUI renderer %s", Logger::Error, err.what());
			return false;
		}
	}

	{
		ProfilerScope $1("Loading common GUI elements");
		// Load GUI style for common elements
		g_commonGUIStyle = std::make_shared<CommonGUIStyle>(m_skin);
	}

	// Create root canvas
	g_rootCanvas = std::make_shared<Canvas>();

	return true;
}

void Application::m_MainLoop()
{
	Timer appTimer;
	m_lastRenderTime = 0.0f;
	while (true)
	{
		// Process changes in the list of items
		bool restoreTop = false;
		for (auto& ch : m_tickableChanges)
		{
			if (ch.mode == TickableChange::Added)
			{
				assert(ch.tickable);
				if (!ch.tickable->DoInit())
				{
					Logf("Failed to add IApplicationTickable", Logger::Error);
					continue;
				}

				if (!m_tickables.empty())
					m_tickables.back()->m_Suspend();

				m_tickables.push_back(std::move(ch.tickable));

				restoreTop = true;
			}
			else if (ch.mode == TickableChange::Removed)
			{
				assert(!m_tickables.empty());
				m_tickables.back()->m_Suspend();
				m_tickables.pop_back();
				restoreTop = true;
			}
		}

		if (restoreTop && !m_tickables.empty())
			m_tickables.back()->m_Restore();

		// Application should end, no more active screens
		if (!m_tickableChanges.empty() && m_tickables.empty())
		{
			Logf("No more IApplicationTickables, shutting down", Logger::Warning);
			return;
		}
		m_tickableChanges.clear();

		// Determine target tick rates for update and render
		const int targetFPS = g_gameConfig.GetInt(GameConfigKeys::FPSTarget);
		float targetRenderTime = 0.0f;
		//for (auto tickable : g_tickables)
		//{
		//	int32 tempTarget = 0;
		//	if (tickable->GetTickRate(tempTarget))
		//	{
		//		targetFPS = tempTarget;
		//	}
		//}
		if (targetFPS > 0)
			targetRenderTime = 1.0f / static_cast<float>(targetFPS);

		// Main loop
		const float currentTime = appTimer.SecondsAsFloat();
		float timeSinceRender = currentTime - m_lastRenderTime;
		
		if (timeSinceRender > targetRenderTime)
		{
			g_avgRenderDelta = g_avgRenderDelta * 0.75f + timeSinceRender * 0.25f; // Calculate avg

			m_deltaTime = timeSinceRender;
			m_lastRenderTime = currentTime;

			// Set time in render state
			m_renderStateBase.time = currentTime;

			// Also update window in render loop
			if (!g_gameWindow->Update())
				return;

			m_Tick();
			timeSinceRender = 0.0f;
		}

		// Tick job sheduler
		// processed callbacks for finished tasks
		g_jobSheduler->Update();

		if (timeSinceRender < targetRenderTime)
		{
			float timeLeft = (targetRenderTime - timeSinceRender);
			uint32 sleepMicroSecs = (uint32)(timeLeft * 1000000.0f * 0.75f);
			std::this_thread::sleep_for(std::chrono::microseconds(sleepMicroSecs));
		}
	}
}

void Application::m_Tick()
{
	// Handle input first
	g_input.Update(m_deltaTime);

	// Tick front tickable
	if(!m_tickables.empty())
		m_tickables.back()->Tick(m_deltaTime);

	// Not minimized / Valid resolution
	if (g_resolution.x > 0 && g_resolution.y > 0)
	{
		glClearColor(0, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT);

		// Render front tickable
		if(!m_tickables.empty())
			m_tickables.back()->Render(m_deltaTime);

		// Time to render GUI
		g_guiRenderer->render(m_deltaTime, Rect(Vector2(0, 0), g_resolution), static_cast<GUIElementBase*>(g_rootCanvas.get()));

		// Swap buffers
		g_gl->SwapBuffers();
	}
}

void Application::Shutdown()
{
	g_gameWindow->Close();
}

void Application::AddTickable(unique_ptr<IApplicationTickable> tickable)
{
	TickableChange change;
	change.mode = TickableChange::Added;
	change.tickable = std::move(tickable);
	m_tickableChanges.push_back(std::move(change));
}

void Application::RemoveTickable()
{
	TickableChange change;
	change.mode = TickableChange::Removed;
	m_tickableChanges.push_back(std::move(change));
}

String Application::GetCurrentMapPath() const
{
	return m_lastMapPath;
}

const Vector<String>& Application::GetAppCommandLine() const
{
	return m_commandLine;
}

RenderState Application::GetRenderStateBase() const
{
	return m_renderStateBase;
}

/**
 * \throws std::runtime_error if failed to create Image
 */
Image Application::LoadImage(const String& name) const
{
	String path = String("skins/") + m_skin + String("/textures/") + name;
	return make_shared<ImageRes>(path);
}

/**
 * \throws std::runtime_error if failed to create Image
 */
Image Application::LoadImageExternal(const String& name) const
{
	return make_shared<ImageRes>(name);
}

/**
 * \throws std::runtime_error if failed to create Texture
 */
Texture Application::LoadTexture(const String& name) const
{
	Texture ret = make_shared<TextureRes>(LoadImage(name));
	return ret;
}

/**
 * \throws std::runtime_error if failed to create Texture
 */
Texture Application::LoadTexture(const String& name, const bool& external) const
{
	if (external)
	{
		Texture ret = make_shared<TextureRes>(LoadImageExternal(name));
		return ret;
	}
	Texture ret = make_shared<TextureRes>(LoadImage(name));
	return ret;
}

/**
 * \throws std::runtime_error if failed to create Material
 * \throws std::runtime_error if failed to create shader
 */
Material Application::LoadMaterial(const String& name) const
{
	String pathV = String("skins/") + m_skin + String("/shaders/") + name + ".vs";
	String pathF = String("skins/") + m_skin + String("/shaders/") + name + ".fs";
	String pathG = String("skins/") + m_skin + String("/shaders/") + name + ".gs";
	Material ret = make_shared<MaterialRes>(g_gl, pathV, pathF);
	// Additionally load geometry shader
	if (Path::FileExists(pathG))
	{
		Shader gshader = make_shared<ShaderRes>(g_gl, ShaderType::Geometry, pathG);
		assert(gshader);
		ret->AssignShader(ShaderType::Geometry, gshader);
	}
	assert(ret);
	return ret;
}

Sample Application::LoadSample(const String& name, const bool& external) const
{
	String path;
	if (external)
		path = name;
	else
		path = String("skins/") + m_skin + String("/audio/") + name + ".wav";

	Sample ret = g_audio->CreateSample(path);
	assert(ret);
	return ret;
}

float Application::GetRenderFPS() const
{
	return 1.0f / g_avgRenderDelta;
}

Transform Application::GetGUIProjection()
{
	return ProjectionMatrix::CreateOrthographic(0.0f, static_cast<float>(g_resolution.x), static_cast<float>(g_resolution.y), 0.0f, 0.0f, 100.0f);
}

void Application::m_OnKeyPressed(int32 key)
{
	// Fullscreen toggle
	if (key == SDLK_RETURN)
	{
		if ((g_gameWindow->GetModifierKeys() & ModifierKeys::Alt) == ModifierKeys::Alt)
		{
			g_gameWindow->SwitchFullscreen();
			g_gameConfig.Set(GameConfigKeys::Fullscreen, g_gameWindow->IsFullscreen());
			return;
		}
	}

	// Pass key to application
	for (const auto& it = m_tickables.rbegin(); it != m_tickables.rend();)
	{
		(*it)->OnKeyPressed(key);
		break;
	}

	//what if only top tickable should get input?
	//if(!m_tickables.empty())
	//	m_tickables.back()->OnKeyPressed(key);
}

void Application::m_OnKeyReleased(int32 key)
{	
	//what if only top tickable should get input?
	//if(!m_tickables.empty())
	//	m_tickables.back()->OnKeyReleased(key);

	for (const auto& it = m_tickables.rbegin(); it != m_tickables.rend();)
	{
		(*it)->OnKeyReleased(key);
		break;
	}
}

void Application::m_OnWindowResized(const Vector2i& newSize)
{
	g_resolution = newSize;
	g_aspectRatio = (float)g_resolution.x / (float)g_resolution.y;

	m_renderStateBase.aspectRatio = g_aspectRatio;
	m_renderStateBase.viewportSize = g_resolution;
	glViewport(0, 0, newSize.x, newSize.y);
	glScissor(0, 0, newSize.x, newSize.y);

	// Set in config
	g_gameConfig.Set(GameConfigKeys::ScreenWidth, newSize.x);
	g_gameConfig.Set(GameConfigKeys::ScreenHeight, newSize.y);
}
