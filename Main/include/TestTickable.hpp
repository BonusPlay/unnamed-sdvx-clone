#pragma once
#include "ApplicationTickable.hpp"
#include "Graphics/Gamepad.hpp"
#include "GUI/SettingsBar.hpp"

/* 
	Simple test scene that can be freely used when developing new graphics features or other things
	this scene is used when -test is provided at application startup
*/
class TestTickable : public IApplicationTickable
{
public:
	bool Init() override;
	void TestTickable::OnKeyPressed(int32 key) override;

	static void StaticFunc(int32 arg)
	{}

	static int32 StaticFunc1(int32 arg)
	{
		return arg * 2;
	}

	static int32 StaticFunc2(int32 arg)
	{
		return arg * 2;
	}

private:
	shared_ptr<CommonGUIStyle> m_guiStyle;
	shared_ptr<SettingsBar> m_settings;

	WString m_currentText;
	float a = 0.1f; // 0 - 1
	float b = 2.0f; // 0 - 10
	float c = 1.0f; // 0 - 5
	float d = 0.0f; // -2 - 2
	int e = 0;
	shared_ptr<Gamepad> m_gamepad;
	Vector<String> m_textSettings;
};
