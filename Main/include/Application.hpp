#pragma once
#include <Audio/Sample.hpp>
#include "ApplicationTickable.hpp"

class Application
{
public:
	Application();
	~Application();

	// Runs the application
	bool Run();
	static void Shutdown();

	void SetCommandLine(int32 argc, char** argv);
	void SetCommandLine(const char* cmdLine);

	void AddTickable(unique_ptr<IApplicationTickable> tickable);
	void RemoveTickable();

	// Current running map path (full file path)
	String GetCurrentMapPath() const;

	// Retrieves application command line parameters
	const Vector<String>& GetAppCommandLine() const;

	// Gets a basic template for a render state, with all the application variables initialized
	RenderState GetRenderStateBase() const;

	// HACK: should not overwrite winapi
#ifdef LoadImage
#undef LoadImage
#endif
	Image LoadImage(const String& name) const;
	Image LoadImageExternal(const String& name) const;
	Texture LoadTexture(const String& name) const;
	Texture LoadTexture(const String& name, const bool& external) const;
	Material LoadMaterial(const String& name) const;
	Sample LoadSample(const String& name, const bool& external = false) const;

	float GetAppTime() const
	{
		return m_lastRenderTime;
	}

	float GetRenderFPS() const;

	static Transform GetGUIProjection();

private:
	bool m_LoadConfig();
	void m_SaveConfig();

	bool m_Init();
	void m_MainLoop();
	void m_Tick();

	void m_OnKeyPressed(int32 key);
	void m_OnKeyReleased(int32 key);
	void m_OnWindowResized(const Vector2i& newSize);

	Vector<unique_ptr<IApplicationTickable>> m_tickables;

	// List of changes applied to the collection of tickables
	// Applied at the end of each main loop
	Vector<struct TickableChange> m_tickableChanges;

	RenderState m_renderStateBase;
	Vector<String> m_commandLine;

	String m_lastMapPath;
	class Beatmap* m_currentMap = nullptr;

	float m_lastRenderTime;
	float m_deltaTime;
	bool m_allowMapConversion;
	String m_skin;
};
